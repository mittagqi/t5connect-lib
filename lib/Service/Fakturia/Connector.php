<?php

namespace Lib\Service\Fakturia;

use Httpful;
use Httpful\Mime;
use Httpful\Request;
use Httpful\Exception\ConnectionErrorException;
use Lib\App\AppException;

/**
 * Connect to Fakturia by its REST-API
 * For details see: https://confluence.luminea.de/display/FAKTURIAWIKI/REST+API 
 */
class Connector {
    
    /**
     * The key to acces the Fakturia API-URL
     * @var string
     */
    protected $apiKey = NULL;
    
    /**
     * The URL where the Fakturia API is found
     * @var string
     */
    protected $apiUrl = NULL;
    
    
    public function __construct($apiKey, $apiUrl) {
        $this->apiKey = $apiKey;
        $this->apiUrl = $apiUrl;
        
        // access-controll to FAKTURA is made by transfering an apiKey on each request.
        // this is done by using a httpfull template
        $template = Request::init()->addHeader('api-key', $this->apiKey);
        Request::ini($template);
        
        if (!$this->testConnection()) {
            throw new ConnectionErrorException('.. not able to connect to Fakturia under "'.$apiUrl.'"');
        }
    }
    
    /**
     * test connection to Fakturia by calling GET('/authtest');
     * @return Boolean
     */
    public function testConnection() : bool {
        try {
            $response = Request::get($this->apiUrl.'/authtest')->send();
            return ($response->hasErrors() || $response->code =! 200) ? FALSE : TRUE;
        }
        catch (ConnectionErrorException $e) {
            return FALSE;
        }
    }
    
    /**
     * get all projects from Fakturia
     * @throws AppException
     * @return \stdClass
     * 
     */
    public function getProjects() : array {
        $response = Request::get($this->apiUrl.'/Projects')->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting projects from Fakturia', $response);
        }
        
        return $response->body;
    }
    
    /**
     * get all contracts from Fakturia
     * @throws AppException
     * @return \stdClass
     *
     */
    public function getContracts() : array {
        $response = Request::get($this->apiUrl.'/Contracts')->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting contracts from Fakturia', $response);
        }
        
        return $response->body;
    }
    
    /**
     * get one contract with ID $contractNumber from Fakturia
     * @throws AppException
     * @return \stdClass
     *
     */
    public function getContract($contractNumber) : \stdClass {
        $response = Request::get($this->apiUrl.'/Contracts/'.$contractNumber)->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting contract with number "'.$contractNumber.'" from Fakturia', $response);
        }
        
        return $response->body;
    }
    
    /**
     * get the prices of a contract with ID $contractNumber from Fakturia
     *
     * @param $contractNumber
     * @param $date
     * @return \stdClass
     * @throws AppException
     * @throws ConnectionErrorException
     */
    public function getContractPrices($contractNumber, string $date = null) : \stdClass {
        $requestExtension = (!is_null($date)) ? '?date='.$date : '';
        $response = Request::get($this->apiUrl.'/Contracts/'.$contractNumber.'/prices'.$requestExtension)->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting prices of contract with number "'.$contractNumber.'" from Fakturia', $response);
        }
        
        return $response->body;
    }
    
    /**
     * get subscription of a contract with ID $contractNumber from Fakturia
     * @throws AppException
     * @return array
     *
     */
    public function getContractSubscription($contractNumber) : \stdClass {
        $response = Request::get($this->apiUrl.'/Contracts/'.$contractNumber.'/Subscription')->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting subscription of contract with number "'.$contractNumber.'" from Fakturia', $response);
        }
        
        return $response->body;
    }
    
    public function getContractActivitiesUnbilled($contractNumber) : array {
        //$response = Request::get($this->apiUrl.'/Contracts/'.$contractNumber.'/Activities?dateFrom='.date('Y-m-d'))->send();
        $response = Request::get($this->apiUrl.'/Contracts/'.$contractNumber.'/Activities')->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting Activities of contract with number "'.$contractNumber.'" from Fakturia', $response);
        }
    
        // we only want the activities, that are not billed yet !
        $tempReturn = [];
        foreach($response->body as $item) {
            if (empty($item->billed)) {
                $tempReturn[] = $item;
            }
        }
        
        return $tempReturn;
    }
    
    /**
     * get the XYZ of a contract with ID $contractNumber from Fakturia
     * @throws AppException
     * @return array
     *
     */
    public function getContractXYZ($contractNumber) : array {
        $response = Request::get($this->apiUrl.'/Contracts/'.$contractNumber.'/ContractCommissionRules')->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting XYZ of contract with number "'.$contractNumber.'" from Fakturia', $response);
        }
        
        return $response->body;
    }
    /**
     * get one item (Artikel) with ID $itemNumber from Fakturia
     * @throws AppException
     * @return \stdClass
     *
     */
    public function getItem($itemNumber) : \stdClass {
        $response = Request::get($this->apiUrl.'/Items/'.$itemNumber)->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting prices of contract with number "'.$contractNumber.'" from Fakturia', $response);
        }
        
        return $response->body;
    }
    
    
    /**
     * Get all open invoices from Fakturia.
     * @return array
     * @throws AppException
     * @throws ConnectionErrorException
     */
    public function getUnpaidInvoices() : array {
        $response = Request::get($this->apiUrl.'/Invoices?unpaidOnly=1')->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting unpaid invoices', $response);
        }
        
        // sort all invoices by "dueDate"
        usort($response->body, self::sortResponseByField('dueDate'));
        
        return $response->body;
    }
    
    /**
     * Mark invoice as paid
     * @param string $invoiceNumber
     * @param string $payDate
     * @param float $amount
     * @return \stdClass
     * @throws AppException
     * @throws ConnectionErrorException
     */
    public function markInvoiceAsPaid(string $invoiceNumber, string $payDate, float $amount) : \stdClass {
        $params = [
            'payDate' => $payDate,
            'payAmount' => $amount
        ];
        $response = Request::post($this->apiUrl.'/Invoices/'.$invoiceNumber.'/Pay')
            //->sendsType(Mime::FORM)
            ->sendsType(Mime::JSON)
            ->body(json_encode($params))
            ->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on mark invoice with number "'.$invoiceNumber.'" as payed '.print_r($params, true), $response);
        }
    
        return $response->body;
    }
    
    
    /**
     * get invoce with ID $invoiceNumber from Fakturia
     * @param integer $invoiceNumber
     * @return \stdClass
     * @throws AppException
     * @throws ConnectionErrorException
     */
    public function getInvoice($invoiceNumber) : \stdClass {
        $response = Request::get($this->apiUrl.'/Invoices/'.$invoiceNumber)->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting invoice with number "'.$invoiceNumber.'"', $response);
        }
        
        return $response->body;
    }
    
    /**
     * get all invoces from Fakturia that belong to contract $contractNumber
     * @param integer $contractNumber
     * @return \stdClass
     * @throws AppException
     * @throws ConnectionErrorException
     */
    public function getInvoicesContract($contractNumber) : array {
        // Alle Filter-Möglichkeiten:
        // /Invoices?customerNumber=K1234&contractNumber=12&dateFrom=2020-01-01&dateTo=2020-02-28&currency=EUR&documentDeliveryMode=EMAIL&unpaidOnly=true&extendedData=true
        $response = Request::get($this->apiUrl.'/Invoices?contractNumber='.$contractNumber)->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting invoices of contract "'.contractNumber.'"', $response);
        }
        // sort invoices by their number
        usort($response->body, self::sortResponseByField('number'));
        
        return $response->body;
    }
    
    /**
     * get all invoces from Fakturia for customer $customerNumber
     * @param $customerNumber
     * @return \stdClass
     * @throws AppException
     * @throws ConnectionErrorException
     */
    public function getInvoicesCustomer($customerNumber) : array {
        // Alle Filter-Möglichkeiten:
        // /Invoices?customerNumber=K1234&contractNumber=12&dateFrom=2020-01-01&dateTo=2020-02-28&currency=EUR&documentDeliveryMode=EMAIL&unpaidOnly=true&extendedData=true
        $response = Request::get($this->apiUrl.'/Invoices?customerNumber='.$customerNumber)->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting invoices of customer "'.$customerNumber.'"', $response);
        }
        // sort invoices by their number
        usort($response->body, self::sortResponseByField('number'));
    
        return $response->body;
    }
    
    /**
     * get all customers from Fakturia
     * @throws AppException
     * @return \stdClass[]
     */
    public function getCustomers() : array {
        $response = Request::get($this->apiUrl.'/Customers')->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting customers from Fakturia', $response);
        }
        // sort customers by their companyName
        usort($response->body, self::sortResponseByField('companyName'));
        
        return $response->body;
    }
    
    /**
     * get customer with number $customerNumber from Fakturia
     * 
     * @param String $customerNumber
     * @throws AppException
     * @return \stdClass
     */
    public function getCustomer($customerNumber) : \stdClass {
        $response = Request::get($this->apiUrl.'/Customers/'.$customerNumber)->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting customer with number '.$customerNumber.' from Fakturia', $response);
        }
        
        return $response->body;
    }
    
    /**
     * get all contracts of customer with number $customerNumber from Fakturia
     * @param string $customerNumber
     * @throws AppException
     * @return \stdClass
     */
    public function getCustomerContracts($customerNumber) : array {
        $response = Request::get($this->apiUrl.'/Customers/'.$customerNumber.'/Contracts')->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting contracts from Fakturia', $response);
        }
        
        return $response->body;
    }
    
    /**
     * put (save) the customer to Fakturia
     * 
     * @param \stdClass $data
     * @throws AppException
     * @return bool
     */
    public function putCustomer($data) : bool {
        $customerNumber = $data->customerNumber;
        $response = Request::put($this->apiUrl.'/Customers/'.$customerNumber)
            ->sendsType(Mime::JSON)
            ->body((array) $data)
            ->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on putting customer with number '.$customerNumber.' to Fakturia', $response);
        }
        
        return TRUE;
    }
    
    /**
     * get user (session) login for $userName from Fakturia
     * 
     * @param String $userName
     * @throws AppException
     * @return \stdClass
     */
    public function getUserLogin($userName) : \stdClass {
        $response = Request::get($this->apiUrl.'/UserLogins/'.$userName.'/sessionToken')->send();
        if ($response->hasErrors()) {
            throw new AppException('Error on getting session-login for user "'.$userName.'" from Fakturia', $response);
        }
        
        return $response->body;
    }
    
    /**
     * helper function to sort invoices by the submitted $key aka fieldname
     * @param $key
     * @return \Closure
     */
    public static function sortResponseByField($key) {
        return function ($a, $b) use ($key) {
            if (!isset($a->$key) || is_null($a->$key)) {return -1;}
            if (!isset($b->$key) || is_null($b->$key)) {return 1;}
            if (is_null($a->$key) && is_null($b->$key)) {return 0;}
            
            return strnatcasecmp($a->$key, $b->$key);
        };
    }
    
    /**
     * 
     * @param string $param
     * @return String
     */
    public function devtest($param): String {
        return 'Fakturia-Connector -> devtest(); with param: '.$param;
    }
}

