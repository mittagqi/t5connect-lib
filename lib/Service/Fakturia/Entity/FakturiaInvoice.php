<?php

namespace Lib\Service\Fakturia\Entity;

use Lib\Service\Fakturia\Connector;

/**
 * Entity for a Fakturia Invoice
 * !! this is not representing all data submitted by Fakturia, only the ones we need.
 */
class FakturiaInvoice {
    
    /**
     * store the complete data from initFromData
     * @var stdClass
     */
    protected $rawData = NULL;
    
    /**
     * The number of this invoice
     * Sample: 'RE1062'
     * @var string
     */
    public $number = '';
    
    /**
     * The number of the customer this invoice belongs to
     * Sample: 'K1015'
     * @var string
     */
    public $customerNumber = '';
    
    /**
     * The number of the account this invoice belongs to
     * Sample: 123
     * @var integer
     */
    public $accountNumber= NULL;
    
    /**
     * The number of the contract this invoice belongs to
     * Sample: 1234
     * @var integer
     */
    public $contractNumber = '';
    
    
    /**
     * The date of this invoice
     * Sample: '2021-05-12'
     * @var string
     */
    public $date = '';
    
    /**
     * The date of this invoice must be payed (Fälligkeitsdatum)
     * Sample: '2021-05-19'
     * @var string
     */
    public $dueDate = '';
    
    /**
     * The date this invoice get payed (Datum des Zahlungseingangs)
     * Sample: '2021-05-15'
     * @var string
     */
    public $settlementDate = '';
    
    
    /**
     * The net-amount of this invoice (excl. tax)
     * Sample: 1,000.1
     * @var float
     */
    public $amountNet = 0;
    
    /**
     * The amount of this invoice to be payed by the customer (incl. tax)
     * Sample: 1,190.12
     * @var float
     */
    public $amountGross = 0;
    
    
    /**
     * indicates if this invoice is canceled
     * @var bool
     */
    protected $canceled = FALSE;
    
    /**
     * indicates if this invoice is a forecast-invoice
     * @var bool
     */
    protected $isForcastInvoice = FALSE;
    
    /**
     * prefix used to generate the invoice-number of forecast invoices
     * @var string
     */
    public static $forecastNumberPrefix = 'F';
    
    /**
     * prefix of invioces as set in Fakturia
     * @var string
     */
    public static $invoiceNumberPrefix = 'RE';
    
    
    /**
     * init a contract from a stdClass $data object
     * @param \stdClass $data
     * @return FakturiaInvoice
     */
    public static function initFromData (\stdClass $data) : FakturiaInvoice {
        $tempSelf = new self();
        //$tempSelf->rawData = $data;
        $tempSelf->number = $data->number;
        $tempSelf->customerNumber = $data->customerNumber;
        
        $tempSelf->accountNumber = $data->accountNumber;
        $tempSelf->contractNumber = $data->contractNumber;
        
        $tempSelf->date = (string) $data->date;
        $tempSelf->dueDate = (string) $data->dueDate;
        $tempSelf->settlementDate = (string) $data->settlementDate;
    
        $tempSelf->amountNet = $data->amountNet;
        $tempSelf->amountGross = $data->amountGross;
        
        $tempSelf->canceled = (isset($data->canceled)) ? (bool) $data->canceled : FALSE;
        
        return $tempSelf;
    }
    
    /**
     * create a forecast-invoice
     *
     * @param FakturiaContract $contract
     * @param int $date
     * @return FakturiaInvoice|null
    public static function initForForecast(FakturiaContract $contract, int $date) : FakturiaInvoice|null {
        // create a new empty invoice
        $tempInvoice = new self();
        
        // set data from $contract
        $tempInvoice->customerNumber = $contract->customerNumber;
        $tempInvoice->accountNumber = $contract->accountNumber;
        $tempInvoice->contractNumber = $contract->contractNumber;
        
        // set amounts
        $tempInvoice->amountNet = $contract->getCalculatedPriceNet($date);
        // if amountNet is 0, aka there is nothing to be billed.. we return NULL cause an invoice with amount 0 is not needed for forecast
        if ($tempInvoice->amountNet == 0) {
            return NULL;
        }
        // depending on the customers taxRegion which is responsible if the customer must pay tax or not
        $tempInvoice->amountGross = ($contract->getCustomerMustPayTax()) ? $contract->getCalculatedPriceGross($date) : $tempInvoice->amountNet;
        
        // set dates
        $tempInvoice->date = date('Y-m-d', $date);
        $tempDueDateModifier = $contract->getDueDateModifier();
        $tempInvoice->dueDate = (strlen($tempDueDateModifier) == 0) ? $tempInvoice->date : date('Y-m-d', strtotime($tempDueDateModifier, $date));
        
        // set forecast invoice-number
        $tempInvoice->number = $tempInvoice->getForecastInvoiceNumber();
        
        // settlementDate must always be empty, cause forcast invoices are not payed yet
        $tempInvoice->settlementDate = '';
        
        // this is a forecast-invoice
        $tempInvoice->isForcastInvoice = TRUE;
        
        
        return $tempInvoice;
    }
     */
    
    /**
     * create a forecast-invoice
     *
     * @param FakturiaContract $contract
     * @param int $date
     * @return FakturiaInvoice|null
     */
    public static function initForForecastBasedOnFakturia(FakturiaContract $contract, int $date, Connector $fc) : FakturiaInvoice|null {
        // create a new invoice based on the contract-informations
        $tempInvoice = self::initBasedOnContract($contract, $date);
        
        // set amounts, we get the forecast from Fakturia
        $tempDueDateModifier = $contract->getDueDateModifier();
        $contract->setContractItems($fc->getContractPrices($contract->contractNumber, date('Y-m-d', strtotime($tempDueDateModifier, $date))));
        $tempInvoice->amountNet = $contract->getCalculatedPriceNet($date);
        // if amountNet is 0, aka there is nothing to be billed.. we return NULL cause an invoice with amount 0 is not needed for forecast
        if ($tempInvoice->amountNet == 0) {
            return NULL;
        }
        // depending on the customers taxRegion which is responsible if the customer must pay tax or not
        $tempInvoice->amountGross = ($contract->getCustomerMustPayTax()) ? $contract->getCalculatedPriceGross($date) : $tempInvoice->amountNet;
        
        
        
        return $tempInvoice;
    }
    
    public static function initBasedOnContract(FakturiaContract $contract, int $date) {
        // creat a new, empty invoice
        $tempInvoice = new self();
        
        // set data from $contract
        $tempInvoice->customerNumber = $contract->customerNumber;
        $tempInvoice->accountNumber = $contract->accountNumber;
        $tempInvoice->contractNumber = $contract->contractNumber;
    
        // set dates
        // first we have to detect the billing date, which is the submitted date
        // or the "nextBillingDate" from the contract (if there is a date defined, and this date is after the submitted date)
        $tempInvoice->date = date('Y-m-d', $date);
        $tempBillingDate = ($contract->nextBilling > $tempInvoice->date) ? $tempBillingDate = $contract->nextBilling : $tempInvoice->date;
        
        // now we have to check if there is a dueDate Modifier for this contract/customer (German: Zahlungsziel)
        // can be something like "10 DAY"
        $tempDueDateModifier = $contract->getDueDateModifier();
        if (strlen($tempDueDateModifier) == 0) {
            $tempInvoice->dueDate = $tempBillingDate;
        }
        else {
            $tempBillingDateTimestamp = strtotime($tempBillingDate);
            $tempInvoice->dueDate = date('Y-m-d', strtotime($tempDueDateModifier, $tempBillingDateTimestamp));
        }
        /*
        error_log(__FILE__.'::'.__LINE__.'; '.__CLASS__.' -> '.__FUNCTION__.';'."\n"
            .' $tempInvoice->date: '.$tempInvoice->date."\n"
            .' vs. $contract->nextBilling: '.$contract->nextBilling."\n"
            .' => $tempBillingDate: '.$tempBillingDate."\n"
            .' + $tempDueDateModifier: '.$tempDueDateModifier."\n"
            .' => $tempInvoice->dueDate: '.$tempInvoice->dueDate);
        */

        // set forecast invoice-number
        $tempInvoice->number = $tempInvoice->getForecastInvoiceNumber();
    
        // settlementDate must always be empty, cause forcast invoices are not payed yet
        $tempInvoice->settlementDate = '';
    
        // this is a forecast-invoice
        $tempInvoice->isForcastInvoice = TRUE;
    
    
        return $tempInvoice;
    }
    
    /**
     * return the generated number used as invoice-number for forecast-invoices.
     * Sample: 'F-K316-2022-07-13'
     * @return string
     */
    protected function getForecastInvoiceNumber() :string {
        return self::$forecastNumberPrefix.'-'.$this->customerNumber.'-'.$this->contractNumber.'-'.$this->date;
    }
    
    /**
     * check if this invoce is a forecast-invoice
     * @return bool
     */
    public function isForecastInvoice() :bool {
        return $this->isForcastInvoice;
    }
    
    /**
     * check if this invoce is canceled
     * @return bool
     */
    public function isCanceled() :bool {
        return $this->canceled;
    }
    
    /**
     * render data to output while dev...
     */
    public function renderTempDev() : string {
        $tempCustomer = FakturiaCustomerList::getCustomer($this->customerNumber);
        
        $tempReturn = "\t\t".'Rechnung ['.$this->number.']:'
            ."\t".'netto: '.$this->amountNet
            ."\t".'brutto: '.$this->amountGross
            ."\t".'date: '.$this->date
            ."\t".'dueDate: '.$this->dueDate
            ."\t".'settlementDate: '.$this->settlementDate
            //."\t".'contractNumber: '.$this->contractNumber
            //."\t".'accountNumber: '.$this->accountNumber
            ."\t".'customer: '.$tempCustomer->companyName;
            
        
        return $tempReturn;
    }
}

