<?php

namespace Lib\Service\Fakturia\Entity;

use Lib\App\App;
use Lib\Logger\Logger;
use Lib\Service\Fakturia\Connector;

/**
 * Entity for a Fakturia Contract
 * !! this is not representing all data submitted by Fakturia, only the ones we need.
 */
class FakturiaContract {
    
    /**
     *
     */
    const STATUS_ACTIVE = 'ACTIVE';
    
    /**
     * store the complete data from initFromData
     * @var stdClass
     */
    protected $rawData = NULL;
    
    /**
     * The ID of this contract
     * Sample: 123
     * @var integer
     */
    public $contractNumber = NULL;
    
    /**
     * The name this contract
     * Sample: 'Support and development contract'
     * @var string
     */
    public $name = '';
    
    /**
     * The number of the account this contract belongs to
     * Sample: 123
     * @var integer
     */
    public $accountNumber = NULL;
    
    /**
     * The number of the customer this contract belongs to
     * Sample: 'K1015'
     * @var string
     */
    public $customerNumber = '';
    
    /**
     * indicates if this constract is active or not
     * Sample: 'ACTIVE'
     * [ DRAFT, ACTIVE, TERMINATED, CANCELLED, ENDED, UPGRADED ]
     * @var string
     */
    public $contractStatus = '';
    
    /**
     * the number of time-units ($recurUni) for this contract (needed together with $noticeUnit)
     * untill the next billing is settled
     * Sample: '3'
     * @var integer
     */
    public $recur = 0;
    /**
     * time-unit for this contract (needed together with $recur)
     * untill the next billing is settled
     * Sample: 'MONTH'
     * @var string
     */
    public $recurUnit = '';
    
    /**
     * the number of time-units ($dueUnit) until this invoice must be payed (needed together with $dueUnit)
     * Sample: '10'
     * @var string
     */
    public $duePeriod = '';
    /**
     * time-unit for this invoice until it must be payed (needed together with $duePeriod)
     * Sample: 'DAY'
     * @var string
     */
    public $dueUnit = '';
    
    /**
     * the date the last billing was send
     * Sample: '2022-09-01'
     * @var string
     */
    public $lastBilling = '';
    
    /**
     * the date the next billing will/must be send
     * Sample: '2022-10-01'
     * @var string
     */
    public $nextBilling = '';
    
    /**
     * if contract is canceled (contractStatus 'TERMINATED') this field holds the date the contract ends
     * @var string
     */
    public $endDate = '';
    
    /**
     * @var FakturiaContractSubscription
     */
    public $subscription = NULL;
    
    /**
     * the list of contract items this contract includes. Needed to calculate the the price of the forecast invoices
     * @var FakturiaContractItem[]
     */
    protected $contractItems = NULL;
    
    /**
     * some customers must not pay their invoices direct, but after a certain time.
     * To caculate the dueDate of the forecast invoices correct, we need this modifier.
     * Sample: '45 DAY'
     * @var null
     */
    protected $customerDueDateModifier = '';
    
    /**
     * some customers need to pay tax, others dont. this depends on the taxRegion thee cusomer lives.
     * To caculate the ammountGross of the forecast invoices correct, we need this information.
     * Sample: TRUE
     * @var bool
     */
    protected $customerMustPayTax = TRUE;
    
    /**
     * init a contract from a stdClass $data object
     * @param \stdClass $data
     * @return FakturiaContract
     */
    public static function initFromData (\stdClass $data) {
        $tempSelf = new self();
        //$tempSelf->rawData = $data;
        $tempSelf->contractNumber = $data->contractNumber;
        $tempSelf->name = $data->name;
        $tempSelf->accountNumber = $data->accountNumber;
        $tempSelf->customerNumber = $data->customerNumber;
        $tempSelf->contractStatus = $data->contractStatus;
        
        $tempSelf->recur = $data->recur;
        $tempSelf->recurUnit = $data->recurUnit;
        
        $tempSelf->duePeriod = $data->duePeriod;
        $tempSelf->dueUnit = $data->dueUnit;
    
        $tempSelf->lastBilling = $data->lastBilling;
        $tempSelf->nextBilling = $data->nextBilling;
        
        $tempSelf->endDate = $data->endDate;
        
        if (is_object($data->subscription)) {
            $tempSelf->subscription = FakturiaContractSubscription::initFromData($data->subscription);
        }
        
        return $tempSelf;
    }
    
    /**
     * test if contract is active,
     * $this->status == 'ACTIVE' or 'TERMINATED'
     * @return bool
     */
    public function isActive() {
        //return in_array($this->contractStatus, ['ACTIVE']);
        return in_array($this->contractStatus, ['ACTIVE', 'TERMINATED']);
    }
    
    /**
     * test if this contract is forecastable.
     * if so, the contract must have a subscription (ABO: "Ja") and the subscription must be 'active'
     * @return bool
     */
    public function isForecastable() {
        // if contract is not active or has no subscripton (Abo: "Nein"), its not forecastable
        if (!$this->isActive() || !$this->subscription || !$this->nextBilling) {
            return FALSE;
        }
        
        return $this->subscription->isActive();
    }
    
    /**
     * set the items of this contract wich are needed to calculate the prices of the forecast-invoices
     * @param $data
     * @return void
     */
    public function setContractItems($data) {
        if (!isset($data->items) || empty($data->items)) {
            return;
        }
        $this->contractItems = [];
        
        foreach ($data->items as $item) {
            $this->contractItems[] = FakturiaContractItem::initFromData($item);
        }
    }
    
    /**
     * some customers must not pay their invoices direct, but after a certain time.
     * To caculate the dueDate of the forecast invoices correct, we need this modifier.
     * Sample: '45 DAY'
     * @param string $dueDateModifier
     * @return void
     */
    public function setCustomerDueDateModifier(string $dueDateModifier) {
        $this->customerDueDateModifier = $dueDateModifier;
    }
    
    /**
     * get the dueDateModifier of this contract.
     * if no special setting wehre made here, the customersDueDateModifier will be returned
     * SAMPLE: '10 DAY'
     * @return string
     */
    public function getDueDateModifier() {
        if (strlen($this->duePeriod.$this->dueUnit) > 0) {
            return $this->duePeriod.' '.$this->dueUnit;
        }
        return $this->customerDueDateModifier;
        
    }
    
    /**
     * some customers need to pay tax, others dont. this depends on the taxRegion thee cusomer lives.
     * To caculate the ammountGross of the forecast invoices correct, we need this information.
     * @param bool $customerMustPayTax
     * @return void
     */
    public function setCustomerMustPayTax(bool $customerMustPayTax) {
        $this->customerMustPayTax = $customerMustPayTax;
    }
    
    /**
     * some customers need to pay tax, others dont. this depends on the taxRegion thee cusomer lives.
     * To caculate the ammountGross of the forecast invoices correct, we need this information.
     * @return bool
     */
    public function getCustomerMustPayTax() {
        return $this->customerMustPayTax;
    }
    
    /**
     * sometimes the prices in the forecast-invoice (which is simply the last invoice)
     * are not set correct.
     * Therfore we set it here right from the contract itself
     * @param int $date
     * @param string $netOrGross
     * @return float
     */
    protected function calculateForecastInvoicePrices(int $date, string $netOrGross) {
        $tempCalculatedPriceNet = 0;
        $tempCalculatedPriceGross = 0;
        foreach($this->contractItems as $contractItem) {
            // only calculate active item
            if ($contractItem->status != self::STATUS_ACTIVE) {
                continue;
            }
            
            // only calculate items that where not billed already
            if (strtotime($contractItem->billedTo) > $date) {
                continue;
            }
            
            // sometimes a contract is not billed every month, but e.g. every 3 months,
            // but contains "monthly" items. Therefore we need a "timeperiod-factor"
            if ($contractItem->unit == $this->recurUnit) {
                $timeperiodFactor = $this->recur;
            }
            else {
                Logger::logError('.. in contract with ID '.$this->contractNumber.'item "'.$contractItem->itemNumber.'"has a different unit!!'."\n"
                    .'contract-unit: '.$this->recurUnit.' vs. item-unit: '.$contractItem->unit
                );
                // as fallback we will charge this item with factor 1
                $timeperiodFactor = 1;
            }
            
            $tempCalculatedPriceNet     += $timeperiodFactor * $contractItem->quantity*$contractItem->priceNet;
            $tempCalculatedPriceGross   += $timeperiodFactor * $contractItem->quantity*$contractItem->priceGross;
        }
        switch($netOrGross) {
            case 'NET':
                return $tempCalculatedPriceNet;
            break;
    
            case 'GROSS':
                return $tempCalculatedPriceGross;
            break;
            
            default:
                echo '!!! invalid argument for $netOrGross in calculateForecastInvoicePrices'; exit;
        }
    }
    
    /**
     * get the calculated net-price (price without tax) for this contract.
     * !!! setContractItems() must be set before !!!
     * @param int $date
     * @return float
     */
    public function getCalculatedPriceNet(int $date) {
        return $this->calculateForecastInvoicePrices($date, 'NET');
    }
    
    /**
     * get the calculated gross-price (price incl. tax )for this contract.
     * !!! setContractItems() must be set before !!!
     * @param int $date
     * @return float|null
     */
    public function getCalculatedPriceGross(int $date) {
        return $this->calculateForecastInvoicePrices($date, 'GROSS');
    }
    
    /**
     * get all forecast invoices until the date $endTime
     * as a list of FakturiaInvoices.
     * Sample (if ):
     * getForecastInvoices() =>
     * [ FakturiaInvoice[number=>@TODO, ..., date=dueDate=>, ...],
     * ]
     * @param Connector $fc
     * @return FakturiaInvoice[]
     * @throws \Httpful\Exception\ConnectionErrorException
     * @throws \Lib\App\AppException
     */
    public function getForecastInvoices(Connector $fc) : array {
        $tempReturn = [];
        
        // if contract is not forecastable, no forecast-invoices can/must be created
        if (!$this->isForecastable()) {
            return $tempReturn;
        }
        
        // make forecast for 12 month in future from now on @TODO: define "endTime" or "forecast-period" somewhere as constant.
        $dateEndTime = strtotime('12 month', strtotime(date('Y').'-'.date('m').'-01'));
        // if contract is terminated, an end-date is set for this contract.
        // in this case we only make forecast until this end-date is reached.
        if (!empty($this->endDate)) {
            $dateEndTime = strtotime($this->endDate);
        }
        $dateNextBilling = strtotime($this->nextBilling);
        
        $iterationStep = 0;
        while(($dateNextInvoice = strtotime($iterationStep*$this->recur.' '.$this->recurUnit, $dateNextBilling)) <= $dateEndTime) {
            // if there is a forcast invoice, we add it to the return array
            if ($tempForcastInvoice = FakturiaInvoice::initForForecastBasedOnFakturia($this, $dateNextInvoice, $fc)) {
                $tempReturn[] = $tempForcastInvoice;
            }
            $iterationStep++;
        };
        
        
        return $tempReturn;
    }
}

/**
 * Entity for a Fakturia Contract Subscription
 * !! this is not representing all data submitted by Fakturia, only the ones we need.
 */
class FakturiaContractSubscription {
    
    /**
     * the status of the subscription
     * Sample: 'ACTIVE'
     * [ DRAFT, ACTIVE, PAUSED, NOCREDIT, ENDED ]
     *
     * @var string
     */
    public $status = NULL;
    
    /**
     * init a contract subscription from a stdClass $data object
     * @param \stdClass $data
     * @return FakturiaContractSubscription
     */
    public static function initFromData (\stdClass $data) {
        $tempSelf = new self();
        $tempSelf->status = $data->status;
        
        return $tempSelf;
    }
    
    /**
     * test if subscription is active,
     * $this->status == 'ACTIVE'
     * @return bool
     */
    public function isActive() {
        return in_array($this->status, [FakturiaContract::STATUS_ACTIVE]);
    }
}