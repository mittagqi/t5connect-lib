<?php

namespace Lib\Service\Fakturia\Entity;

/**
 * Entity for a Fakturia Contract Item
 * !! this is not representing all data submitted by Fakturia, only the ones we need.
 */
class FakturiaContractItem
{
    /** the "number" (ID) of this item.
     * Sample: 'translate5_51'
     * @var string
     */
    public $itemNumber = '';
    
    /**
     * indicates of this item must be added to the price-calculation.
     * Only item with status 'ACTIVE' must be noted
     * @var string
     */
    public $status = '';
    
    /**
     * the timebased untiy of this item.
     * Sample: 'MONTH'
     * @var string
     */
    public $unit = '';
    
    /**
     * the number how often this item is subscribed
     * ?? @TODO: dont know if this should be a float.. aka.. if 1.5 should be possible
     * @var integer
     */
    public $quantity = 1;
    
    /**
     * The price of this item (net without tax)
     * Sample: 149,90
     * @var float
     */
    public $priceNet = 0;
    
    /**
     * The price of this item (gross incl. tax)
     * Sample: 187,50
     * @var float
     */
    public $priceGross = 0;
    
    /**
     * The tax-rate of this item
     * Sample: 19
     * @var float
     */
    public $taxRate = 0;
    
    /**
     * the date untill this item is already billed to.
     * Sample: '2022-10-01'
     * @var string
     */
    public $billedTo = '';
    
    /**
     * init a contract item from a stdClass $data object
     * @param \stdClass $data
     * @return FakturiaContractItem
     */
    public static function initFromData(\stdClass $data)
    {
        $tempSelf = new self();
        $tempSelf->itemNumber = $data->itemNumber;
        $tempSelf->status = $data->status;
        $tempSelf->unit = $data->unit;
        $tempSelf->quantity = $data->quantity;
        $tempSelf->priceNet = $data->priceNet;
        $tempSelf->priceGross = $data->priceGross;
        $tempSelf->taxRate = $data->taxRate;
        $tempSelf->billedTo = $data->billedTo;
        
        return $tempSelf;
    }
    
    /**
     * test if item is active,
     * $this->status == 'ACTIVE'
     * @return bool
     */
    public function isActive() {
        return in_array($this->status, ['ACTIVE']);
    }
    
}