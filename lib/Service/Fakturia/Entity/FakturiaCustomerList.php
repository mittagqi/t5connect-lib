<?php

namespace Lib\Service\Fakturia\Entity;

use Lib\App\App;
use Lib\Service\Fakturia\Connector;

/**
 * A list of all Fakturia Customers
 * so customers can be used as "cached list" which must only be loaded once from the Fakturia-Server
 */
class FakturiaCustomerList {
    
    /**
     * The list of stored FakturiaCustomers.
     * Key of the List is customerNumber, so a certain customer can easily be found by its number.
     * @var FakturiaCustomer[]
     */
    public static $list = NULL;
    
    // to avoide that this class is used with "new FakturiaCustomerList()" some functions a declared as private
    private function __construct() {}
    private function __clone() {}

    
    /**
     * load the list of customers from the Fakturia server
     * @throws \Lib\App\AppException
     */
    protected static function loadCustomersFromServer() {
        // if list is already loaded, nothing else need to be done
        if (!is_null(self::$list)) {
            return;
        }
    
        $fc = new Connector(App::config()->Fakturia->ApiKey, App::config()->Fakturia->ApiUrl);
        foreach ($fc->getCustomers() as $tempCustomerData) {
            $tempCustomer = FakturiaCustomer::initFromData($tempCustomerData);
            self::$list[$tempCustomer->customerNumber] = $tempCustomer;
        }
    }
    
    /**
     * get the complete list of FakturiaCustomers
     * @return FakturiaCustomer[]
     */
    public static function getList() {
        self::loadCustomersFromServer();
        return self::$list;
    }
    /**
     * get a FakturiaCustomer with number $customerNumber
     * @param $customerNumber
     * @return FakturiaCustomer|void
     */
    public static function getCustomer($customerNumber) {
        self::loadCustomersFromServer();
        
        if (isset(self::$list[$customerNumber])) {
            return self::$list[$customerNumber];
        }
        
        return NULL;
    }
}

