<?php

namespace Lib\Service\Fakturia\Entity;

/**
 * Entity for a Fakturia Customer
 * !! this is not representing all data submitted by Fakturia, only the ones we need.
 */
class FakturiaCustomer {
    
    /**
     * store the complete data from initFromData
     * @var stdClass
     */
    protected $rawData = NULL;
    
    /**
     * The number of this customer
     * Sample: 'K1015'
     * @var string
     */
    public $customerNumber = '';
    
    /**
     * The company name
     * Sample: 'Stephan'
     * @var string
     */
    public $firstName = '';
    
    /**
     * The company name
     * Sample: 'Bergmann'
     * @var string
     */
    public $lastName = '';
    
    /**
     * The company name
     * Sample: 'K1015'
     * @var string
     */
    public $companyName = '';
    
    /**
     * The registered mail adress
     * Sample: 'info@company.de'
     * @var string
     */
    public $email = '';
    
    /**
     * duePeriod together with dueUnit is needed to calculate thje dueDate for this customer.
     * Sample: $duePeriod = 10; $dueUnit = 'DAY' => '10 DAY'
     * @var string
     */
    public $duePeriod = '';
    public $dueUnit = '';
    
    /**
     * indicates the tax-region this customer belongs to. Important to calculate the forcast-invoices if there should be tax included or not.
     * possible Values: 'NATIONAL', 'EU', 'NONEU'
     * @var string
     */
    protected $taxRegion = '';
    
    
    /**
     * init a customer from a stdClass $data object
     * @param \stdClass $data
     * @return FakturiaCustomer
     */
    public static function initFromData (\stdClass $data) {
        $tempSelf = new self();
        //$tempSelf->rawData = $data;
        $tempSelf->customerNumber = $data->customerNumber;
        $tempSelf->firstName = $data->firstName;
        $tempSelf->lastName = $data->lastName;
        $tempSelf->companyName = $data->companyName;
        $tempSelf->email = $data->email;
    
        $tempSelf->duePeriod = $data->duePeriod;
        $tempSelf->dueUnit = $data->dueUnit;
    
        $tempSelf->taxRegion = $data->taxRegion;
        
        return $tempSelf;
    }
    
    /**
     * get the dueDateModifier for this customer;
     * SAMPLE: '10 DAY'
     * @return string
     */
    public function getDueDateModifier() {
        if (strlen($this->duePeriod.$this->dueUnit) < 1) {
            return '';
        }
        return $this->duePeriod.' '.$this->dueUnit;
    }
    
    /**
     * some customers do not need to pay tax cause they are in a taxRegion where no tax mus be charged.
     * @return bool
     */
    public function mustPayTax() {
        if (in_array($this->taxRegion, ['EU', 'NONEU'])) {
            return FALSE;
        }
        
        return TRUE;
    }
}

