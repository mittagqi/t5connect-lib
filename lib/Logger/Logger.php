<?php

namespace Lib\Logger;

use Lib\App\App;
use Lib\Mailer\Mailer;

/**
 * Logger class.
 * @author sb
 */
class Logger {
    
    
    /**
     * The directory in which the logger will be saved (inside the application folder)
     * @var string
     */
    const FOLDERNAME_LOGGER = '/log';
    
    /**
     * inside the general logger directory each log has its own directory to store the data inside.
     * this $name is the name of the directory
     * @var string
     */
    protected $name;
    
    
    /**
     * The complete path to the logger directory
     * @var string
     */
    protected $path;
    
    
    /**
     * the max number of entries inside a log.
     * an entry herby stands for all messages of one day.
     * @var integer
     */
    protected $maxEntries;
    
    /**
     * Indicates the log-entry will be send to the admin by mail
     * @var bool
     */
    protected $sendMessage2Admin = FALSE;
    
    /**
     * Indicates if every log-entry should have an "header"
     * @var bool
     */
    protected $entriesWithHeader = FALSE;
    
    
    
    /**
     * Log the message $message to the error-logger
     * @param string $message
     */
    public static function logError($message) {
        $tempLogger = new self('error', 30, TRUE);
        $tempLogger->log($message);
    }
    
    /**
     * Log the message $message to the error-logger
     * @param string $message
     */
    public static function logActivity($message) {
        $tempLogger = new self('activity', 30);
        $tempLogger->log($message);
    }
    
    /**
     * Log the message $message to the notification-logger
     * @param string $message
     */
    public static function logNotification($message) {
        $tempLogger = new self('notification', 30);
        $tempLogger->log($message);
    }
    
    /**
     * Log the message $message to the info-logger
     * @param string $message
     */
    public static function logInfo($message) {
        $tempLogger = new self('info', 30);
        $tempLogger->log($message);
    }
    
    /**
     * Log the message $message to the accessDenied-logger
     * @param string $message
     */
    public static function logAccessDenied($message) {
        $tempLogger = new self('accessDenied', 10);
        $tempLogger->log($message);
    }
    
    /**
     * Log the message $message to the mail-logger
     * @param string $message
     */
    public static function logMails($message) {
        $tempLogger = new self('mails', 30);
        $tempLogger->log($message);
    }
    
    
    /**
     * The logger $name will be created inside the general logger folder.
     * This logger will hold $maxEntries which means that older entries will be deleted.
     * an entry inside a logger holds all mesages of one day.
     * @param string $name
     */
    protected function __construct($name, $maxEntries, bool $sendMessage2Admin = FALSE, bool $entriesWithHeader = FALSE) {
        $this->name = $name;
        $this->path = App::getRootFolder().self::FOLDERNAME_LOGGER.'/'.$name;
        
        $this->maxEntries = $maxEntries;
        $this->sendMessage2Admin = $sendMessage2Admin;
        $this->entriesWithHeader = $entriesWithHeader;
        
        $this->createLogDirectory();
        $this->deleteOldLogEntries();
    }
    
    
    /**
     * Write $message into the logger
     * @param string $message
     */
    protected function log($message) {
        // always replace passwords in log-text @TODO: reactivate this function
        // $message = Tools::removePasswordsFromText($message);
        
        try {
            if ($this->entriesWithHeader) {
                $message = "==============================================================".PHP_EOL.
                        date('Y-m-d H:i:s').PHP_EOL.
                        "==============================================================".PHP_EOL.PHP_EOL.
                        $message.
                        PHP_EOL.PHP_EOL.PHP_EOL;
            }
            else {
                $message = date('Y-m-d H:i:s').': '.$message.PHP_EOL;
            }
            file_put_contents($this->path.'/'.date('Y-m-d').'.log', $message, FILE_APPEND);
            
            // send $message to admin by mail (if necessary)
            if ($this->sendMessage2Admin) {
                Mailer::sendAdmin($message);
            }
        }
        catch (Exception $e) {
            error_log('Log "'.$this->name.'" can not write. Message: '.PHP_EOL.message.PHP_EOL.PHP_EOL.$e->getMessage());
        }
    }
    
    /**
     * create the directory of the log (if not exists already).
     * if container could NOT be created is thrown.
     */
    protected function createLogDirectory() {
        // do nothing if the directory already exists.
        if (is_dir($this->path)) {
            return;
        }
        
        // create the dirctory
        if (mkdir($this->path, 0777, TRUE)) {
            return;
        }
        
        // if creation was not possible, throw an Exception
        throw new Exception('log directory "'.$this->path.'" can not be created');
    }
    
    
    /**
     * Delete the old entries inside the log.
     */
    protected function deleteOldLogEntries() {
        // detect all entries inside the log
        $tempEntries = glob($this->path.'/*.log');
        
        // do nothing if there are less (or equal) than $maxEntries entries
        if (count($tempEntries) <= $this->maxEntries) {
            return;
        }
        
        // sort entries from new to old
        arsort($tempEntries);
        
        // get the entries that needs to be deleted
        $tempEntries = array_slice($tempEntries, $this->maxEntries);
        
        // delete the old entries
        foreach ($tempEntries as $tempEntry) {
            unlink($tempEntry);
        }
    }
}


