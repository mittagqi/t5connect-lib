<?php

namespace Lib\Entity;

use Lib\App\AppException;
use Lib\Db\Db;
use Laminas\Db\ResultSet\ResultSet;
use Lib\App\App;

/**
 * Entity for a list of Translate5-Instances.
 * Designed as a singleton, so there only can be one list.
 */
class T5InstanceList {
    
    /**
     * The list of T5Instances
     * @var T5Instance[]
     */
    protected $T5Instances = NULL;
    
    /**
     * class should not be used with "new" constructor.
     * inset it always one of the public static function should be used.
     */
    protected function __construct() { }
    
    /**
     * Search for $search in the T5Instances DB table.
     * @param string $search
     * @param string $orderField (optional)
     * @return T5Instance[]
     */
    public static function search($search, $orderField = 'id', $onlyShowInstancesFromThisServer = true) {
        // get the instance
        $tempT5InstancesList = new static();
        
        // check if $orderField is a valid fieldname
        if (!array_key_exists($orderField, get_class_vars('Lib\Entity\T5Instance'))) {
            throw new AppException('invalid orderField "'.$orderField.'"');
        }
        
        // check if only isnatnces of the configured server should be shown
        $sqlServerId = ($onlyShowInstancesFromThisServer)
            ? sprintf('serverId=%s AND', DB::escape(App::config()->T5InstanceServer))
            : '';
        
        $sql = sprintf('SELECT * FROM %s WHERE %s (instanceId LIKE %s OR mainDomain LIKE %s) ORDER BY %s',
            T5Instance::getDBTableName(),
            $sqlServerId,
            DB::escape('%'.$search.'%'),
            DB::escape('%'.$search.'%'),
            $orderField
        );
        
        $tempResults = DB::query($sql);
        
        // if no entries are found in the DB table
        if ($tempResults->count() < 1) {
            return $tempT5InstancesList;
        }
        
        // convert db data into a list of T5Instances
        $resultSet = new ResultSet();
        $resultSet->initialize($tempResults);
        foreach ($resultSet->toArray() as $data) {
            $tempT5InstancesList->T5Instances[] = new T5Instance($data);
        }
        
        return $tempT5InstancesList;
    }
    
    /**
     * Get the list of T5Instances ordered by field $orderField
     * @param string $orderField
     * @return T5InstanceList
     */
    public static function loadAllOrdered($orderField = 'mainDomain') {
        return self::search('', $orderField);
    }
    
    /**
     * Get the list of T5Instances ordered by field $orderField
     * @param string $orderField
     * @return T5InstanceList
     */
    public static function loadAllOrderedFromAllServer($orderField = 'mainDomain') {
        return self::search('', orderField: $orderField, onlyShowInstancesFromThisServer: false);
    }
    
    /**
     * @return T5Instances[]|null
     */
    public function getT5Instances() {
        return $this->T5Instances;
    }
    
    /**
     * check if hasOneErrorHealthstate is set
     * @return bool
     */
    public function hasErrorHealthstate() : bool {
        foreach($this->T5Instances as $T5Instance) {
            if ($T5Instance->getHealthState() == T5Instance::HEALTHSTATE_ERROR) {
                return TRUE;
            }
        }
        return FALSE;
    }
}

