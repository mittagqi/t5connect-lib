<?php

namespace Lib\Entity;

use http\Client;
use Laminas\Http\Client\Adapter\Exception\TimeoutException;
use Laminas\Http\Response;
use Lib\Db\Db;
use Lib\App\App;

/**
 * Entity for a Translate5-Instance
 */
class T5Instance {
    
    /**
     * The id of this instance as stored in the db
     * Sample: 1234
     * @var integer
     */
    public $id = NULL;
    
    /**
     * For all automatically created instances, this instanceID is equal to this->id.
     * For manually created instances this is any kind of unique identifier
     * Sample: '/webs/dummie' or 1234
     * @var string
     */
    public $instanceId = '';
    
    
    /**
     * The server this instance is located.
     * Sample: 's1mittag.inetseite.de'
     * @var string
     */
    public $serverId = NULL;
    
    /**
     * The ID of the Fakturia-contract this instance belongs to.
     * Sample: 1234
     * @var integer
     */
    public $fakturiaContractId = NULL;
    
    /**
     * The ID of the Fakturia-customer this instance belongs to.
     * Sample: 'K1000'
     * @var string
     */
    public $fakturiaCustomerId = NULL;
    
    /**
     * the current state of this instance;
     * Sample: 'installed'
     * @var string
     */
    public $state = NULL;
    
    /**
     * Mail-adress to send messages and to be used as initial instance mail-adresse
     * Sample: name@domain.org
     * @var string
     */
    public $email = '';
    
    /**
     * the subdomain under which this instance should be reachable.
     * Sample: 'devTest' for an instance that is reachable under https://devTest.translate5.net
     * @var string
     */
    public $mainDomain = NULL;
    
    /**
     * contains the config of this instance as a json-ified object
     * @var string
     */
    public $config = NULL;
    
    /**
     * contains a simple history of this instance.
     * @var string
     */
    public $history = NULL;
    
    /**
     * contains comments, notes about this instance.
     * @var string
     */
    public $comments = NULL;
    
    /**
     * the password for the Translate5-User 'mittagqi'
     * @var string
     */
    public $mittagqiPassword = NULL;
    
    /**
     * When a single instance wants to communicate with InstanceAutomation, this key must be submitted as credential.
     * The value is a "read-only" value, which is set once if its empty. After that it will not be saved into the DB any more.
     * @var string
     */
    public $instanceCommunicationKey = NULL;
    
    /**
     * decides if this instance should be monitored, aka. applicationstate should be loaded
     * @var string
     */
    public $monitoring = NULL;
    
    /**
     * field to hold the loaded/submitted applicationstate of the instance
     * @var string
     */
    protected $cachedApplicationstate = NULL;
    
    /**
     * timestammp of the content of fiels cachedApplicationstate to determine of stat is still vald/actual
     * @var string
     */
    protected $cachedApplicationstateTimestamp = NULL;
    
    /**
     * the applicationstate of the instance submitted from the instance.
     * concrete applicationstate is always generated from the cachedApplicationstate
     * @var T5InstanceApplicationstate
     */
    protected $applicationstate = NULL;
    
    /**
     * if instance has an health-error, it is usefull to know the reason for the error.
     * This reason can be stored in this field
     * @var string|null
     */
    protected ?string $healthErrorDescription = NULL;
    
    /**
     * The desired time preference of the client (configurable) - the backup is tried to be started as early as possible after that time
     * @var string
     */
    public string $backupDailyPreference = '02:02:00';
    
    /**
     * The last full backup duration in seconds
     * @var int|null
     */
    public ?int $backupFullDuration = 0;
    
    /**
     * The last inc backup duration in seconds
     * @var int|null
     */
    public ?int $backupIncDuration = 0;
    
    /**
     * The calculated timestamp of the next backup start
     * @var string|null
     */
    public ?string $backupStart = null;
    
    /**
     * datetime when a currently running backup should be finished.
     * this is neded to check if maintenance mode, caused by a backup, is still OK
     * @var string|null
     */
    public ?string $backupExpectedEnd = null;
    
    /**
     * to check if maintenance mode is triggered by daily backup
     */
    const BACKUP_MAINTENANCE_MESSAGE = 'Daily backup and update';
    
    /**
     * the different states this instance can have
     * @var string
     */
    const STATE_ORDERED = 'ordered';
    const STATE_INSTALLED = 'installed';
    const STATE_ACTIVE = 'active';
    const STATE_LOCKED = 'locked';
    const STATE_DELETED = 'deleted';
    
    /**
     * the different health-states this instance can have
     * @var string
     */
    const HEALTHSTATE_INACTIVE  = 'INACTIVE';
    const HEALTHSTATE_ERROR     = 'ERROR';
    const HEALTHSTATE_WARNING   = 'WARNING';
    const HEALTHSTATE_OK        = 'OK';
    
    /**
     * The different monitoring states
     * @var string
     */
    const MONITORING_ALL = 'all';
    const MONITORING_NOSTATUS = 'nostatus';
    const MONITORING_NONE = 'none';
    
    
    /**
     * The prefix that ist used e.g. for the name of the folder for an instance.
     * Sample:
     * instance with ->id == 1234 is located in a folder 'instance-1234'
     * @var string
     */
    const INSTANCE_PREFIX = 'instance-';
    
    /**
     * The table name the instances are stored in the database
     * @var string
     */
    protected static $dbTableName = 'T5Instances';
    
    /**
     * get the name of the table where the T5Instances are stored in the databse
     * @return string
     */
    public static function getDBTableName() : string {
        return self::$dbTableName;
    }
    
    
    
    /**
     * init a new T5Instance with the submitted $data
     * @param array $data
     */
    public function __construct($data = NULL) {
        //set state default
        if(empty($data['state'])) {
            $data['state'] = self::STATE_ORDERED;
        }
        foreach($data as $field => $value) {
            if(property_exists($this, $field)) {
                $this->$field = $value;
            }
        }
    }
    
    /**
     * Load the instance with id $id from the database
     * If there is no instance in the database, null will be returned.
     *
     * @param integer $fakturiaContractId
     * @return T5Instance|null
     */
    public static function loadById($id) : ?T5Instance {
        return self::loadByFieldValue('id', (integer) $id);
    }
    
    /**
     * Load the instance with $fakturiaContractId from the database
     * If there is no instance in the database, null will be returned.
     *
     * @param integer $fakturiaContractId
     * @return T5Instance|null
     */
    public static function loadByFakturiaContractId($fakturiaContractId) : ?T5Instance{
        return self::loadByFieldValue('fakturiaContractId', (integer) $fakturiaContractId);
    }

    /**
     * Load the instance with instanceId $instanceId from the database
     * If there is no instance in the database, null will be returned.
     *
     * @param string $instanceId
     * @return T5Instance|null
     */
    public static function loadByInstanceId($instanceId) : ?T5Instance {
        return self::loadByFieldValue('instanceId', $instanceId);
    }

    /**
     * Load the instance with mainDomain $mainDomain from the database
     * If there is no instance in the database, null will be returned.
     *
     * @param string $instanceId
     * @return T5Instance|null
     */
    public static function loadByMainDomain($mainDomain) : ?T5Instance {
        return self::loadByFieldValue('mainDomain', $mainDomain);
    }
    
    
    /**
     * Load one instance where $field has $value from the database
     * If there is no instance in the database, null will be returned
     * If multiple instances are found, the first will be returned. So be carefull what you do!
     *
     * @param integer $fakturiaContractId
     * @return ?T5Instance
     */
    protected static function loadByFieldValue($field, $value) : ?T5Instance {
        $sql = sprintf('SELECT * FROM %s WHERE %s=%s',
            self::$dbTableName,
            $field,
            DB::escape($value)
        );
        if (!$tempData = Db::getRow($sql)) {
            return null;
        }
        return new self($tempData);
    }
    
    
    /**
     * Create a T5InstanceJob that belongs to this T5Instance.
     * So here the fields ->instanceId and ->serverId are already set to the correct values in the returned T5InstanceJob
     * @return T5InstanceJob
     */
    public function getNewInstanceJob() : T5InstanceJob {
        $tempJob = new T5InstanceJob();
        $tempJob->instanceId = $this->instanceId;
        $tempJob->serverId = $this->serverId;
        
        return $tempJob;
    }
    
    /**
     * Returns the path to the instance webroot
     * @param string $instanceId
     * @return string
     */
    public static function getWebRoot(string $instanceId) : string {
        if(self::isLegacy($instanceId)) {
            return $instanceId;
        }
        return App::config()->webRoot.self::INSTANCE_PREFIX.$instanceId;
    }
    
    /**
     * Change the T5Instance with $instanceId to the state $newState
     * @param string $instanceId
     * @param string $newState
     * @return bool false if no instance to be updated found
     */
    public static function changeState($instanceId, $newState): bool {
        $tempT5Instance = self::loadByInstanceId($instanceId);
        if(empty($tempT5Instance)) {
            return false;
        }
        $tempT5Instance->state = $newState;
        $tempT5Instance->save();
        return true;
    }
    
    
    /**
     * returns true if the given ID is of a legacy (manually installed) installation
     * @param string $id
     * @return boolean
     */
    public static function isLegacy($id): bool {
        return substr($id,0,1) === '/';
    }
    
    /**
     * A instance id may only numeric, or a string with alpha numeric values
     * @param string $id
     * @return bool
     */
    public static function isValidInstanceId(string $id): bool {
        return (bool) preg_match('#^[a-zA-Z0-9]+$#', $id);
    }
    
    /**
     * Currently only not IDNs are supported, see https://stackoverflow.com/a/16491074/1749200 for the regex
     * @param string $domain
     * @return boolean
     */
    public static function isValidDomain(string $domain) {
        return (bool) preg_match('#^(?!\-)(?:(?:[a-zA-Z\d][a-zA-Z\d\-]{0,61})?[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$#', $domain);
    }
    
    
    /**
     * save the T5Instance entity to the database.
     * return TRUE if everything was OK, FALSE if an error occured while saving the entity into the database.
     * If optional parameter $forceInsert is set, the entity will always be inserted and not updated.
     * This is needed for Instance-Migration, where an id is already set from external.
     *
     * @return bool
     */
    public function save($forceInsert = FALSE) : bool {
        // if no password for the mittagQI-user is configured yet, create a new one
        if (!$this->mittagqiPassword) {
            $this->mittagqiPassword = 'use session:impersonate!';
        }
        
        // if no server-ID is configured yet, set the configured default
        if (!$this->serverId) {
            $this->serverId = App::config()->T5InstanceServer;
        }

        $fieldsToSave = [
            'instanceId',
            'serverId',
            'fakturiaCustomerId',
            'fakturiaContractId',
            'state',
            'email',
            'mainDomain',
            'config',
            'history',
            'comments',
            'mittagqiPassword',
            'cachedApplicationstate',
            'cachedApplicationstateTimestamp',
            'backupDailyPreference',
            'backupFullDuration',
            'backupIncDuration',
            'backupStart',
        ];

        $tempFields = [];
        foreach($fieldsToSave as $field) {
            if(is_null($this->$field)) {
                $tempFields[] = $field.' = null';
            }
            else {
                $tempFields[] = $field.' = '.Db::escape($this->$field);
            }
        }

        // write all current settings into a fields-sql, all fields except 'id'
        $tempFields = join(', ', $tempFields);
        
        if (!$this->instanceCommunicationKey) {
            // @TODO: maybe a central createPasswort will be usefull
            $this->instanceCommunicationKey = bin2hex(random_bytes(20));
            $tempFields .= sprintf(', instanceCommunicationKey=%s', Db::escape($this->instanceCommunicationKey));
        }
        
        if ($forceInsert == TRUE) {
            $tempFields .= sprintf(', id=%d', $this->id);
        }
        
        // create new entry in database
        if (!$this->id || $forceInsert == TRUE) {
            $sql = sprintf('INSERT INTO %s SET %s', self::$dbTableName, $tempFields);
            if (!$tempResult = Db::query($sql)) {
                return FALSE;
            }
            $this->id = $tempResult->getGeneratedValue();
            
            // instanceId for new created entities is equal to the id of the instance (if it is empty)
            if (!$this->instanceId) {
                $this->instanceId = $this->id;
                $this->save();
            }
        }
        
        // or update an existing entry
        else {
            $sql = sprintf('UPDATE %s SET %s WHERE id=%d', self::$dbTableName, $tempFields, $this->id);
            if (!Db::query($sql)) {
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
    /**
     * delete the T5Instance entity from the database.
     * return TRUE if everything was OK, FALSE if an error occured while deleting the entity from the database.
     * @return boolean
     */
    public function delete() : bool {
        // do nothing if current object has no ID (was never saved before)
        if (!$this->id) {
            return TRUE;
        }
        
        $sql = sprintf('DELETE FROM %s WHERE id=%d', self::$dbTableName, $this->id);
        Db::query($sql);
        return TRUE;
    }
    
    /**
     * add a new entry into the history
     * @param string $message
     */
    public function addHistoryEntry($message) {
        $this->history .= date('Y-d-m H:i:s ').$message."\n\n";
    }
    
    /**
     * add a new entry into the comments
     * @param string $message
     */
    public function addCommentEntry($message) {
        $this->comments .= date('Y-d-m H:i:s ').$message."\n\n";
    }
    
    /**
     * While some instances do not have set a value in mainDomain
     * this function will generate a fallback name "tempDomain <instanceId>"
     * @return mixed|string|null
     */
    public function getDomain() {
        return (!empty($this->mainDomain)) ? $this->mainDomain : 'tempDomain '.$this->instanceId;
    }
    
    
    /**
     * will load the instances status site from the live site under
     * 'https://'.$this->mainDomain.'/editor/index/applicationstate/'<br>
     * and store this information in $this->liveHttpStatusCode, $this->liveStatusRawData and $this->liveStatusData
     */
    protected function loadApplicationstate() : void {
        // do nothing else if the applicationstate was loaded before
        if (!is_null($this->applicationstate)) {
            return;
        }
    
        // only active instances can have an valid applicationstate
        if ($this->state != self::STATE_ACTIVE) {
            return;
        }
        
        // for instances with monitoring != 'all' no applications should be detected
        if ($this->monitoring != T5Instance::MONITORING_ALL) {
            return;
        }
        
        // if cachedApplicationstate-data are available, and this data are not too old,
        // take this data for applicationstate.
        // @TODO: @SBE:
        if (!empty($this->cachedApplicationstate)) {
            $this->applicationstate = T5InstanceApplicationstate::initFromJson($this->cachedApplicationstate);
            return;
        }
        
        // @TODO.. temp loading the applicationstate
        return;
        
        // if there are no (actual) stored applicationstate, load it from the server
        $this->loadAppliactionstateFromInstanceServer();
    }
    
    /**
     * load the instance status directly form the instance server by calling "/editor/index/applicationstate/'"
     * @return void
     */
    protected function loadAppliactionstateFromInstanceServer() : void {
        try {
            // @TODO: hopefully all installations will have https (not only http)
            $statusUrl = 'https://'.$this->mainDomain.'/editor/index/applicationstate/';
        
            $client = new \Laminas\Http\Client(
                $statusUrl,
                [
                    'maxredirects' => 0,
                    'timeout'      => 5,
                
                    // the following lines are mainly here cause else verification of local, selfsigned zertificates will fail
                    'sslverifypeer' => FALSE,
                    'sslallowselfsigned' => TRUE,
                    'sslverifypeername' => FALSE,
                ]
            );
            $response = Response::fromString( $client->send());
        
            $tempMatches = [];
            $this->applicationstate = T5InstanceApplicationstate::initFromJson($response->getBody());
            $this->setCachedApplicationstate((preg_match('#.*<body>(.*?)<\/body>.*#s', $response->getBody(), $tempMatches)) ? $tempMatches[1] : strip_tags($response->getBody()));
            $this->setCachedApplicationstateTimestamp();
            $this->save();
        }
        catch (TimeoutException $e) {
            error_log(__FILE__.'::'.__LINE__.'; '.__CLASS__.' -> '.__FUNCTION__.'; .. not able to load Applicationstate of instance '.$this->getDomain());
        }
    }
    
    /**
     * get the applicastionstate of this instanze as structured data
     * @return T5InstanceApplicationstate|null
     */
    public function getApplicationstate() : T5InstanceApplicationstate|null {
        $this->loadApplicationstate();
        return $this->applicationstate;
    }
    
    /**
     * return the cachedApplicationstate
     * @return mixed|string|null
     */
    public function getCachedApplicationState() : string|null {
        return $this->cachedApplicationstate;
    }
    
    /**
     * set the cachedApplicastionstate of this instanze
     */
    public function setCachedApplicationstate($applicationstate) : void {
        $this->cachedApplicationstate = $applicationstate;
    }
    
    /**
     * return the timestamp of the field cachedApplicationstate
     * which is the time the cachedApplicationstate was last submitted/loaded form the instance server
     * @return mixed|string|null
     */
    public function getCachedApplicationstateTimestamp() : string|null {
        return $this->cachedApplicationstateTimestamp;
    }
    
    /**
     * set the cachedApplicastionstate of this instanze
     */
    public function setCachedApplicationstateTimestamp() : void {
        $this->cachedApplicationstateTimestamp = date('Y-m-d H:i:s');
    }
    
    
    /**
     * detect the health state of this instance.<br>
     * Returns one of the healthstates:<br>
     *  self::HEALTHSTATE_INACTIVE<br>
     *  self::HEALTHSTATE_ERROR<br>
     *  self::HEALTHSTATE_WARNING<br>
     *  self::HEALTHSTATE_OK<br>
     * <br>
     * if an error is returned, a short description is set and can be readout by getHealthErrorDescription()<br>
     *
     * @return string
     */
    public function getHealthState() : string {
        // check for inactive instances
        if ($this->state != T5Instance::STATE_ACTIVE || $this->monitoring != T5Instance::MONITORING_ALL) {
            return self::HEALTHSTATE_INACTIVE;
        }
    
        // check if applicationstate is too old (and not currently running a backup !?!)
        if (strtotime($this->cachedApplicationstateTimestamp) < strtotime('- 30 minutes') && !$this->backupExpectedEnd) {
            $this->setHealthErrorDescription('timestamp of applicationstate is too old');
            return self::HEALTHSTATE_ERROR;
        }
        
        // check if we have a valid applicationstate
        if (!$tempApplicationstate = $this->getApplicationstate()) {
            $this->setHealthErrorDescription('cachedApplicationstate is invalid');
            return self::HEALTHSTATE_ERROR;
        }
        
        // check if daily backup maintenance took to long
        // should not last longer than 20 minutes after backupExpectedEnd
        if ($this->backupExpectedEnd && strtotime('- 20 minutes') < strtotime($this->backupExpectedEnd)) {
            $this->setHealthErrorDescription('maintenance-mode for daily backup took too long');
            return self::HEALTHSTATE_ERROR;
        }
        
        
        // all good if no other case happend before
        // @TODO ?? am not sure if that is correct
        return self::HEALTHSTATE_OK;
        
        
        
        // DEVTEST return random healthstate
        $tempHealthStates = [
            self::HEALTHSTATE_INACTIVE,
            self::HEALTHSTATE_ERROR,
            self::HEALTHSTATE_WARNING,
            self::HEALTHSTATE_OK,
        ];
        //error_log(__FILE__.'::'.__LINE__.'; '.__CLASS__.' -> '.__FUNCTION__.'; '.print_r($tempHealthStates, TRUE)); exit;
        
        return $tempHealthStates[array_rand($tempHealthStates)];
    }
    
    /**
     * if instance has an health-error, it is usefull to know the reason for the error.
     * This reason can be set with this function
     * @param $description
     * @return void
     */
    protected function setHealthErrorDescription($description) : void {
        $this->healthErrorDescription = $description;
    }
    
    /**
     * if instance has an health-error, you can get a short description for the reason.
     * @return string|null
     */
    public function getHealthErrorDescription() : ?string {
        return $this->healthErrorDescription;
    }
}

