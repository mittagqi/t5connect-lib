<?php

namespace Lib\Entity;

use Laminas\Db\Sql\Ddl\Column\Boolean;
use Lib\App\AppException;
use Lib\Db\Db;
use Laminas\Db\ResultSet\ResultSet;
use Lib\App\App;

/**
 * Entity for the status-data of a live-installation
 * At the moment its just a simple DataContainer to structure the informations
 */
class T5InstanceApplicationstate {
    /**
     * @var Boolean
     */
    public $isUptodate = FALSE;
    
    /**
     * @var string
     */
    public $version = 'NULL';
    
    /**
     * @var T5InstanceApplicationstateMaintenance
     */
    public $maintenance = NULL;
    
    /**
     * @var T5InstanceApplicationstateDatabase
     */
    public $database = NULL;
    
    /**
     * @var T5InstanceApplicationstateWorker
     */
    public $worker = NULL;
    
    /**
     * @var array
     */
    public $pluginsLoaded = [];
    
    /**
     * class should only be constructed by calling initFromJson($jsonData)
     * @param $data
     */
    protected function __construct($data) {
        $this->isUptodate = $data['isUptodate'];
        $this->version = $data['version'];
        $this->maintenance = new T5InstanceApplicationstateMaintenance($data['maintenance']);
        $this->database = new T5InstanceApplicationstateDatabase($data['database']);
        $this->worker = new T5InstanceApplicationstateWorker($data['worker']);
        $this->pluginsLoaded = $data['pluginsLoaded'];
    }
    
    /**
     * return a new T5InstanceLiveStatus filled with the json-formated data $jsonData
     */
    public static function initFromJson($jsonData) {
        if (!($json = json_decode($jsonData, TRUE))) {
            return NULL;
        }
        
        return new self($json);
    }
    
}

/**
 * simple DataContainer to hold the applicationstate maintenance structure
 */
class T5InstanceApplicationstateMaintenance {
    public $message = NULL;
    public $startDate = NULL;
    public $timeToNotify = NULL;
    public $timeToLoginLock = NULL;
    public $announcementMail = NULL;
    
    public function __construct($data) {
        $this->message = $data['message'];
        $this->startDate = $data['startDate'];
        $this->timeToNotify = $data['timeToNotify'];
        $this->timeToLoginLock = $data['timeToLoginLock'];
        $this->announcementMail = $data['announcementMail'];
    }
}

/**
 * simple DataContainer to hold the applicationstate database structure
 */
class T5InstanceApplicationstateDatabase {
    public $newCount = NULL;
    public $modCount = NULL;
    public $isUptodate = NULL;
    
    public function __construct($data) {
        $this->newCount = $data['newCount'];
        $this->modCount = $data['modCount'];
        $this->isUptodate = $data['isUptodate'];
    }
}

/**
 * simple DataContainer to hold the applicationstate worker structure
 */
class T5InstanceApplicationstateWorker {
    public $scheduled = NULL;
    public $waiting = NULL;
    public $running = NULL;
    public $defunct = NULL;
    public $done = NULL;
    
    public function __construct($data) {
        $this->scheduled = $data['scheduled'];
        $this->waiting = $data['waiting'];
        $this->running = $data['running'];
        $this->defunct = $data['defunct'];
        $this->done = $data['done'];
    }
}