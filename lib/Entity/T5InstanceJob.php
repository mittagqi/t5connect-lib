<?php

namespace Lib\Entity;

use Lib\App\App;
use Lib\Db\Db;
use Lib\Logger\Logger;
use Lib\App\AppException;
use ServerMaintenance\Instance\ApacheVhost;

/**
 * Entity for a Translate5-Instance-Job
 * This Jobs are the connection to the server-maintenace scripts,, an are stored in the DB-table T5InstanceJobs
 */
class T5InstanceJob {
    
    /**
     * Unique ID of this job (is equal to the PRIMERY key in the DB).
     * @var integer
     */
    protected $id = NULL;
    
    /**
     * For all automatically created instances, this instanceID is equal to this->id.
     * For manually created instances this is any kind of unique identifier
     * Sample: '/webs/dummie' or 1234
     * @var string
     */
    public $instanceId = NULL;
    
    /**
     * The server the instance is located.
     * Sample: 's1mittag.inetseite.de'
     * @var string
     */
    public $serverId = NULL;
    
    /**
     * The action the needs to be done.
     * Sample: 'install'
     * @var string
     */
    public $action = NULL;
    
    /**
     * the different actions this job can have
     * @var string
     */
    const ACTION_INSTALL        = 'install';
    const ACTION_CONFIG         = 'config';
    const ACTION_CONFIGAPACHE   = 'configApache';
    const ACTION_LOCK           = 'lock';
    const ACTION_UNLOCK         = 'unlock';
    const ACTION_DELETE         = 'delete';
    
    
    /**
     * The ID of the Fakturia-customer this instance belongs to.
     * Sample: 'K1000'
     * @var string
     */
    public $parameter = NULL;
    
    /**
     * the current state of this job
     * Sample: 'open'
     * @var string
     */
    public $state = NULL;
    
    /**
     * the timestamp the job-entry was created.
     * the parameter is "readonly", that means that this value created automatically in the databese once
     * and will not be saved into the databse on this->save
     */
    public $dateCreated = NULL;
    
    /**
     * the different states this job can have
     * @var string
     */
    const STATE_OPEN = 'open';
    const STATE_RUNNING = 'running';
    const STATE_DONE = 'done';
    const STATE_ERROR = 'error';
    
    /**
     * The default comment which ist submitted in every shell command
     * @var string
     */
    const DEFAULT_COMMENT = 'set by InstanceAutomation';
    
    
    /**
     * The table name the instance-jobss are stored in the database
     * @var string
     */
    protected static $dbTableName = 'T5InstanceJobs';
    
    /**
     * get the name of the table where the T5InstanceJobs are stored in the databse
     * @return string
     */
    public static function getDBTableName() {
        return self::$dbTableName;
    }
    
    /**
     * When no special phpCli is defined, which is the normal setting, then this will be returned as php cli call
     * @var string
     */
    protected static $fallbackPhpCli = 'php';
    
    /**
     * get the php cli
     * @return string
     */
    public static function getPhpCli() {
        return (!is_null(App::config()->phpCli)) ? App::config()->phpCli : self::$fallbackPhpCli;
    }
    
    
    /**
     * init a new T5InstanceJob with the submitted $data
     * @param array $data
     */
    public function __construct($data = NULL) {
        $this->id           = (isset($data['id'])) ? $data['id'] : NULL;
        $this->instanceId   = (isset($data['instanceId'])) ? $data['instanceId'] : NULL;
        $this->serverId     = (isset($data['serverId'])) ? $data['serverId'] : NULL;
        $this->action       = (isset($data['action'])) ? $data['action'] : NULL;
        $this->parameter    = (isset($data['parameter'])) ? $data['parameter'] : NULL;
        $this->state        = (isset($data['state'])) ? $data['state'] : self::STATE_OPEN;
        $this->dateCreated  = (isset($data['dateCreated'])) ? $data['dateCreated'] : NULL;
    }
    
    /**
     * Check if the current Job is allowed for the submitted $t5InstanceState
     * @param string $t5InstanceState
     */
    public function isJobAllowed($t5InstanceState) {
        $mapAllowedJobActions = [
            T5Instance::STATE_ORDERED   => [self::ACTION_INSTALL,                                                   self::ACTION_DELETE],
            T5Instance::STATE_INSTALLED => [self::ACTION_CONFIG,    self::ACTION_CONFIGAPACHE,                      self::ACTION_DELETE],
            T5Instance::STATE_ACTIVE    => [self::ACTION_CONFIG,    self::ACTION_CONFIGAPACHE,  self::ACTION_LOCK,  self::ACTION_DELETE],
            T5Instance::STATE_LOCKED    => [self::ACTION_UNLOCK,    self::ACTION_CONFIGAPACHE,                      self::ACTION_DELETE],
            T5Instance::STATE_DELETED   => [],
        ];
        
        // check if t5Instance is in a state that is defined in the $mapAllowedJobActions
        if (!key_exists($t5InstanceState, $mapAllowedJobActions)) {
            Logger::logError('unknow t5Instance->state "'.$t5InstanceState.'" !!! check $mapAllowedActions in '.__FILE__.'::'.__LINE__.'; '.__CLASS__.' -> '.__FUNCTION__.';');
            return FALSE;
        }
        
        // check if action of this job is allowed for the $t5InstanceState
        return in_array($this->action, $mapAllowedJobActions[$t5InstanceState]);
    }
    
    /**
     * run the job...
     */
    public function run() {
        // load job params
        $params = json_decode($this->parameter);
        Logger::logActivity('Instance "'.$this->instanceId.'" run job "'.$this->action.'" with params: '.print_r($params, TRUE));
        
        // set state to running
        $this->state = self::STATE_RUNNING;
        $this->save();
        
        switch ($this->action) {
            case self::ACTION_INSTALL:
                $shellScript = self::getPhpCli().' '.App::getRootFolder().'/server-maintenance.php';
                $statements = [$this->getStatementInstall()];
                $this->runShellCommands($shellScript, $statements);
                // if installation is finished, change T5Instance state to 'insalled'
                T5Instance::changeState($this->instanceId, T5Instance::STATE_INSTALLED);
            break;
            
            case self::ACTION_CONFIG:
                $shellScript = T5Instance::getWebRoot($this->instanceId).'/translate5.sh';
               // if a special php cli is set, we share this php as CMD_PHP environment variable
                if (self::getPhpCli() != self::$fallbackPhpCli) {
                    $shellScript = 'CMD_PHP='.self::getPhpCli().' '.$shellScript;
                }
                $statements = $this->getStatementsConfig();
                $this->runShellCommands($shellScript, $statements);
            break;
            
            case self::ACTION_CONFIGAPACHE:
                $this->runConfigApache();
            break;
            
            default:
                $this->state = self::STATE_ERROR;
                $this->save();
                throw new AppException('unknown action "'.$this->action.'" detected in T5InstanceJob with id '.$this->id, $this);
        }
        
        // if everything went well, set state to done
        $this->state = self::STATE_DONE;
        $this->save();
    }
    
    /**
     * run the apache config job,directly not by calling futher shell commands
     */
    protected function runConfigApache() {
        // load job params
        $params = (array) json_decode($this->parameter);
        
        // edit apache config
        $ap = new ApacheVhost();
        $ap->editApacheConfig($this->instanceId, $params);
    }
    
    /**
     * run the given $shellScript with the submittes $statements...
     * while $staements is an array, the concrete commands that will be executed are:
     * 
     * $shellScript $statement[0]
     * $shellScript $statement[1]
     * ...
     * 
     * @param string $shellScript
     * @param array $statements
     * @return NULL
     */
    protected function runShellCommands($shellScript, $statements) {
        // do nothing if no statementes where submitted
        if (empty($statements)) {
            $this->state = self::STATE_ERROR;
            $this->save();
            Logger::logError('empty statements detected in T5InstanceJob with id '.$this->id."\n".print_r($this, TRUE));
        }
        
        foreach ($statements as $statement) {
            $result = [];
            $command = $shellScript.' '.$statement;
            Logger::logActivity('Instance Job run shell command: '.$command);
            // run the command
            exec($command, $result);
            
            // if there is no result, an error occured"
            if (!$result) {
                $this->state = self::STATE_ERROR;
                $this->save();
                throw new AppException('error on InstanceJob run shell command: '.$command, $this);
            }
            // if everything was OK, log the result...
            Logger::logActivity("Result:\n".implode("\n", $result));
        }
    }
    
    /**
     * Get the shell statement to create an instance.
     * will return something like:
     * instance:create "instance1234.translate5.net" "customers name" -e
     * 
     * @return string
     */
    protected function getStatementInstall() {
        // load job params
        $params = json_decode($this->parameter);
        
        // Fallback if company information is not submitted in the job params
        $tempCompany = (!empty($params->companyName)) ? $params->companyName : $params->firstName.' '.$params->lastName;
        
        // detect the needed options for instance creation
        $options = '--instance-id='.escapeshellarg($this->instanceId);
        $options .= ' --email='.escapeshellarg($params->email);
        
        // Do not ask any interactive question
       $options .= ' --no-interaction';
       
        // Instances from InstanceAutomation will use the default plugin-set on intial installation
        $options .= ' -p0';
        
        // and they will use the T5Instance from DB, so no new entry will be generated
        $options .= ' --useInstanceFromDB';
        
        // detect the arguments for instance creation
        $domain = T5Instance::INSTANCE_PREFIX.$this->instanceId.App::config()->domainRoot;
        $company = escapeshellarg($tempCompany);
        
        
        // finally create the complete statement
        return 'instance:create '.$options.' '.$domain.' '.$company;
    }
    
    /**
     * will return a list of translate5 config statements, Sample:
     * [
     *  'instance:config runtimeOptions.maxUserPm 2',
     *  'instance:config runtimeOptions.maxUserEditor 20',
     *  ...
     * ]
     * @return array
     */
    protected function getStatementsConfig() {
        // load job params
        $params = json_decode($this->parameter);
        
        $statements = [];
        foreach($params as $name => $value) {
            // some of the params must be handled in a special way
            switch ($name) {
                case 'plugins.ordered':
                    $tempValue = json_encode($value);
                break;
                
                default:
                    $tempValue = escapeshellarg($value);
            }
            
            $statements[] = 'config runtimeOptions.'.$name.' '.$tempValue.' --comment '.escapeshellarg(self::DEFAULT_COMMENT);;
        }
        
        return $statements;
    }
    
    protected function getStatementLock() {
        
    }
    
    protected function getStatementDelete() {
        
    }
    
    /**
     * save the T5InstanceJob entity to the database.
     * ! "readonly" value dateCreated will not be stored.
     * return TRUE if everything was OK, FALSE if an error occured while saving the entity into the database
     * @return bool
     */
    public function save() {
        // write all current settings into a fields-sql, all fields except 'id'
        $tempFields = sprintf('instanceId=%s, serverId=%s, action=%s, parameter=%s, state=%s',
                
                Db::escape($this->instanceId), Db::escape($this->serverId), Db::escape($this->action), Db::escape($this->parameter), Db::escape($this->state)
        );
        
        // create new entry in database
        if (!$this->id) {
            $sql = sprintf('INSERT INTO %s SET %s', self::$dbTableName, $tempFields);
            if (!$tempResult = Db::query($sql)) {
                return FALSE;
            }
            $this->id = $tempResult->getGeneratedValue();
        }
        
        // or update an existing entry
        else {
            $sql = sprintf('UPDATE %s SET %s WHERE id=%d', self::$dbTableName, $tempFields, $this->id);
            if (!Db::query($sql)) {
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
    /**
     * delete the T5InstanceJob entity from the database.
     * return TRUE if everything was OK, FALSE if an error occured while deleting the entity from the database.
     * @return boolean
     */
    public function delete() {
        // do nothing if current object has no ID (was never saved before)
        if (!$this->id) {
            return TRUE;
        }
        
        $sql = sprintf('DELETE FROM %s WHERE id=%d', self::$dbTableName, $this->id);
        Db::query($sql);
        return TRUE;
    }
    
}

