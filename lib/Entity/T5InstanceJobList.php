<?php

namespace Lib\Entity;

use Lib\Db\Db;
use Laminas\Db\ResultSet\ResultSet;
use Lib\App\App;

/**
 * Entity for a list of Translate5-InstanceJobs
 */
class T5InstanceJobList {
    
    /**
     * Get all T5InstanceJobs that belong to this server
     * @return T5InstanceJob[]
     */
    public static function getAll() {
        return self::getAllFiltered();
    }
    
    /**
     * Get all T5InstanceJobs that belong to this server with state="open"
     * 
     * @return T5InstanceJob[]
     */
    public static function getAllOpen() {
        $filter = sprintf('AND state=%s', DB::escape(T5InstanceJob::STATE_OPEN));
        return self::getAllFiltered($filter);
    }
    
    
    /**
     * Get all T5InstanceJobs that belong to this server
     * 
     * @return T5InstanceJob[]
     */
    protected static function getAllFiltered($filter = NULL) {
        $sql = sprintf('SELECT * FROM %s WHERE serverId=%s %s ORDER BY id', 
                T5InstanceJob::getDBTableName(),
                DB::escape(App::config()->T5InstanceServer),
                $filter
                );
        //error_log(__FILE__.'::'.__LINE__.'; '.__CLASS__.' -> '.__FUNCTION__.'; SQL: '.$sql);
        $tempResults = DB::query($sql);
        
        // if no entries are found in the DB table
        if ($tempResults->count() < 1) {
            return NULL;
        }
        
        // convert db data into a list of T5InstanceJobs
        $tempReturn = [];
        $resultSet = new ResultSet();
        $resultSet->initialize($tempResults);
        foreach ($resultSet->toArray() as $data) {
            $tempReturn[] = new T5InstanceJob($data);
        }
        
        return $tempReturn;
    }
    
}

