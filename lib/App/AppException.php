<?php

namespace Lib\App;

use Lib\Logger\Logger;

class AppException extends \Exception {
    
    public function __construct($text, $additionalInformations = NULL) {
        $tempErrorText = $text;
        $tempErrorText .= (!is_null($additionalInformations)) ? "\n\nAdditional Informations:\n".print_r($additionalInformations, TRUE) : ''; 
        Logger::logError($tempErrorText);
        
        parent::__construct($text);
    }
    
}

