<?php

namespace Lib\App;

use Laminas;
use Lib\Logger\Logger;

/**
 * The App is created by App::init($appName, $rootFolder);
 * This will load the config file /config/{$appName}.php
 * and in addition a /config/{$appName}.local.php file which can overwrite/extend settings from the /config/{$appName}.php 
 */

class App {
    
    /**
     * the name of the app. Must be set by ::init($appName, $rootFolder)
     */
    protected static $appName = NULL;
    
    /**
     * the root-folder of the app. Must be set by ::init($appName, $rootFolder)
     */
    protected static $rootFolder = NULL;
    
    /**
     * the name of the config folder
     * @var string
     */
    const CONFIG_FOLDER = '/config';
    
    /**
     * store once generated config objects in an array with index $appName
     * @var \Laminas\Config\Config[]
     */
    protected static $configCache = NULL;
    
    /**
     * container to store the applications request
     * @var Laminas\Diactoros\ServerRequest
     */
    protected static $request = NULL;
    
    
    /**
     * Prevent class to be used as normal class
     */
    private function __construct() {}
    
    /**
     * init the Application class by submitting the applications name $appName and the 
     * root folder $rootFolder where the root of the application is located.
     */
    public static function init($appName, $rootFolder) {
        self::$appName = $appName;
        self::$rootFolder = realpath($rootFolder);
    }
    
    /**
     * Get the config for self::$appName
     * @return \Laminas\Config\Config
     */
    public static function config() {
        if (is_null(self::$appName)) {
            die('you have to call App::init(\'appName\') first');
        }
        
        // if config of $appName is already in cache, return it instantly
        if (isset(self::$configCache[self::$appName])) {
            return self::$configCache[self::$appName];
        }
        
        // detect the path to the config folder
        $tempConfigPath = self::getRootFolder().self::CONFIG_FOLDER;
        
        // load the self::$appName config file
        $tempAppConfig = \Laminas\Config\Factory::fromFile($tempConfigPath.'/'.self::$appName.'.php', TRUE);
        // if there is a local config file, load and merge it
        if (file_exists($tempFile = $tempConfigPath.'/'.self::$appName.'.local.php')) {
            $tempAppConfig->merge(\Laminas\Config\Factory::fromFile($tempFile, TRUE)); 
        }
        
        // make the complete config readOnly
        $tempAppConfig->setReadOnly();
        
        // cache the config for $appName for later instant-return
        self::$configCache[self::$appName] = $tempAppConfig;
        
        // return the config
        return $tempAppConfig;
    }
    
    /**
     * 
     * @return Laminas\Diactoros\ServerRequest
     */
    public static function getRequest() {
        if (is_null(self::$request)) {
            self::$request = \Laminas\Diactoros\ServerRequestFactory::fromGlobals();
        }
        
        return self::$request;
    }
    
    /**
     * return the path to the root-folder of the application
     * @return string
     */
    public static function getRootFolder() {
        if (is_null(self::$rootFolder)) {
            throw new AppException('rootFolder is not set in App::init !!');
        }
        return self::$rootFolder;
    }
    
    /**
     * return the name of the application
     * @return string
     */
    public static function getName() {
        if (is_null(self::$appName)) {
            die('you have to call App::init(\'appName\') first');
        }
        
        return self::$appName;
    }
    
    /**
     * check if current user (current IP-number) is allowed to reach the "component" $name
     * accessControll must be defined for the "component" $name in the config file(s)
     * 
     * @param string $name
     */
    public static function accessControll($name) {
        if (!$tempConfig = self::config()->accessControll->{$name}) {
            $tempMessage = 'no accessControl configured for "'.$name.'"';
            Logger::logError($tempMessage);
            die($tempMessage);
        }
        
        // check if full access is configured
        if ($tempConfig == '*') {
            return TRUE;
        }
        
        // convert config values to array
        $tempIpNumbers = ($tempConfig instanceof \Laminas\Config\Config) ? $tempConfig->toArray() : (array) $tempConfig;
        
        // check current IP is in the array of configured IP-numbers
        if (in_array($_SERVER['REMOTE_ADDR'], $tempIpNumbers)) {
            return TRUE;
        }
        
        $tempMessage = 'access denied for "'.$name.'"';
        Logger::logAccessDenied($tempMessage."\n".print_r($_SERVER, TRUE));
        die($tempMessage);
    }
    
    /**
     * get the current disk-space usage as percent.
     */
    public static function getDiskspaceUsage() {
        $totalSpace = disk_total_space('/');
        $freeSpace = disk_free_space('/');
        return round((100 * (($totalSpace-$freeSpace) / $totalSpace)), 1);
    }
}