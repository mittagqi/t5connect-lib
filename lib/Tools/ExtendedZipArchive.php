<?php
namespace Lib\Tools;

class ExtendedZipArchive extends \ZipArchive {
    
    protected $addDirRootDir = NULL;
    
    /**
     * add a comlete folder to the zip
     * @param $path
     * @return bool
     */
    public function addDir($path) : bool {
        // do nothing if $path is not a directory
        if (!is_dir($path)) {
            return FALSE;
        }
        if (is_null($this->addDirRootDir)) {
            $this->addDirRootDir = $path;
        }
        
        $this->addEmptyDir($path);
        $nodes = glob($path . '/*');
        foreach ($nodes as $node) {
            if (is_dir($node)) {
                $this->addDir($node);
            } else if (is_file($node)) {
                $this->addFile($node, str_replace($this->addDirRootDir, '', $node));
            }
        }
        
        return TRUE;
    }
}