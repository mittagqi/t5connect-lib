<?php
namespace Lib\Tools;

class DownloadHelper {
    
    public static function getFileFormUrl($url, $target) {
        $ch = curl_init();

        // Checking whether target-file exists or not, this will also create missing directories !!!
        $tempTargetDir = pathinfo($target, PATHINFO_DIRNAME);
        if (!is_dir($tempTargetDir)) {
            mkdir($tempTargetDir, 0777, TRUE);
        }
        // and now open a filehandler to the target-file
        $targetFile = fopen($target, 'w');
        
        
        // check https://www.php.net/manual/de/function.curl-setopt.php for other CURL options
        curl_setopt( $ch, CURLOPT_HEADER, FALSE );
        curl_setopt( $ch, CURLOPT_URL, $url );
        //curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, TRUE );
        //curl_setopt( $ch, CURLOPT_AUTOREFERER, TRUE );
    
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
        curl_setopt( $ch, CURLOPT_HEADER, FALSE ); // must be set to FALSE (which is also default) else repsonse-header will be included in target file)
        
        curl_setopt( $ch, CURLOPT_FILE, $targetFile); // directly write curl-answer into file
    
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
        
        
        $curlResult = curl_exec( $ch );
        
        // check the repsonse (may can be an 401 "UNAUTHORIZED" or whatever)
        $curlInfo = NULL;
        if(!curl_errno($ch)) {
            $curlInfo = curl_getinfo($ch);
        }
        curl_close( $ch );
        fclose($targetFile);
        
        // check if repsonse was valid (code 200)
        if (!$curlInfo || $curlInfo['http_code'] != 200) {
            unlink($target);
            return FALSE;
        }
        //error_log(__FILE__.'::'.__LINE__.'; '.__CLASS__.' -> '.__FUNCTION__.'; '.print_r($curlInfo, TRUE));
        
        return (bool) $curlResult;
    }
}