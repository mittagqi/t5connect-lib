<?php

namespace Lib\Mailer;

use Lib\App\App;
use Lib\Logger\Logger;

/**
 * Mailer class.
 * @author sb
 */
class Mailer {
    /**
     * contains the transport object.
     * colud be one of the possible transport described in https://docs.laminas.dev/laminas-mail/transport/intro/
     * @var \Laminas\Mail\Transport\Sendmail | \Laminas\Mail\Transport\Smtp | ...
     */
    protected $transport = NULL;
    
    /**
     * init tranpsort depending on the App::config
     */
    protected function __construct() {
        switch(App::config()->mail->transport->type) {
            case 'sendmail':
                $this->transport = new \Laminas\Mail\Transport\Sendmail();
            break;
            
            case 'smtp':
                $this->transport = new \Laminas\Mail\Transport\Smtp();
                $tempOptions = new \Laminas\Mail\Transport\SmtpOptions(App::config()->mail->transport->options);
                $this->transport->setOptions($tempOptions);
            break;

            case 'disabled':
                $this->transport = new \Laminas\Mail\Transport\InMemory();
                break;

            default:
                die ('no mail->transport is defined in App::config');
        }
    }
    
    protected function send(\Laminas\Mail\Message $message) {
        // if config->mail->log is TRUE, log mail into /log/$appName/mails
        if (App::config()->mail->log == TRUE) {
            Logger::logMails($message->toString());
        }
        $this->transport->send($message);
    }
    
    public static function test() {
        // create a test-message
        $message = new \Laminas\Mail\Message();
        $message->addFrom(App::config()->adminMail);
        $message->addTo(App::config()->adminMail);
        $message->setSubject('.. Test-Mail from '.App::getName());
        $message->setBody(
'Hello World,

this little mail was send to you by '.App::getName().'


Greetings...
Stephan
');
        
        // and send it
        $tempMailer = new self();
        $tempMailer->send($message);
    }
    
    /**
     * Send $text to the configured config->adminMail
     * @param string $text
     */
    public static function sendAdmin(string $text) {
        $message = new \Laminas\Mail\Message();
        $message->addFrom(App::config()->adminMail);
        $message->addTo(App::config()->adminMail);
        $message->setSubject('.. Error-Log Entry in '.App::getName());
        $message->setBody($text);
        
        // and send it
        $tempMailer = new self();
        $tempMailer->send($message);
    }
}