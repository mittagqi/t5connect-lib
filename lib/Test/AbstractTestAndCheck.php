<?php
/**
 * Abstract class for Tests and Checks
 * 
 * contains already some general tests.
 * more tests and checks can be defined in classes that extend this abstract class
 * 
 * Tests:
 * Each test must be defined as a separate (protected) function in this or the child class(es).
 * The function must have the prefix 'tester' in the function name.
 * 
 *
 * Checks:
 * Each check must be defined as a separate (protected) function in this or the child class(es).
 * The function must have the prefix 'checker' in the function name.
 *
 */

namespace Lib\Test;

use Lib\App\App;
use Lib\Logger\Logger;
use Lib\Mailer\Mailer;

abstract class AbstractTestAndCheck {
    
    /**
     * container for a list of all available tests.
     * @var array
     */
    protected $availableTests = NULL;
    
    /**
     * container for a list of all available checker.
     * @var array
     */
    protected $availableChecker = NULL;
    
    
    public function __construct() {
        // detect all available test. E.G. a function with name testerXyz in this class means that there is a test Xyz available
        $tempAvailableTests = array_filter(get_class_methods(get_class($this)),
                function($name, $key) {
                    return (strpos($name, 'tester') === 0);
                },
                ARRAY_FILTER_USE_BOTH);
        // now cut away the "tester"-prefix of the found function
        array_walk($tempAvailableTests,
                function(& $value, $key) {
                    $value = substr($value, 6);
                }
        );
        
        $this->availableTests = $tempAvailableTests;
        
        
        // detect all available checker. E.G. a function with name checkerXyz in this class means that there is a checker Xyz available
        $tempAvailableChecker = array_filter(get_class_methods(get_class($this)),
                function($name, $key) {
                    return (strpos($name, 'checker') === 0);
                },
                ARRAY_FILTER_USE_BOTH);
        // now cut away the "checker"-prefix of the found function
        array_walk($tempAvailableChecker,
                function(& $value, $key) {
                    $value = substr($value, 7);
                }
        );
        
        $this->availableChecker = $tempAvailableChecker;
    }
    
    /**
     * Determin which test shold be run.
     * e.g. test('CreatePdfFromPptx') will run $this->testCreatePdfFromPptx();
     * 
     * If submitted function is 'all' then all tests will be run
     * 
     * @param string $function
     */
    public function test($function = NULL) {
        switch (TRUE) {
            // run a specific test
            case (in_array($function, $this->availableTests)):
                $this->runTest($function);
            break;
            
            // run all tests
            case ($function == 'all'):
                foreach ($this->availableTests as $function) {
                    $this->runTest($function);
                }
            break;
            
            // test not found, show help
            default:
                echo 'Invalid test "'.$function.'" called !!!'."\n\n".'available tests are:'."\n".'- '.implode("\n- ", $this->availableTests)."\n\n".'If you want to run ALL available tests, call --test=all';
            break;
        }
    }
    
    /**
     * run test function $this->tester{$function}()
     * @param string $function
     */
    protected function runTest($function) {
        echo '-------------------------------------- '."\n";
        echo '.. run test "'.$function.'"'."\n";
        echo '-------------------------------------- '."\n";
        $tempFunctionName = 'tester'.$function;
        $this->{$tempFunctionName}();
        echo "\n\n";
    }
    
    
    /**
     * Determin which checker shold be run.
     * e.g. check('ConfigAcrossWorkflow') will run $this->checkerConfigAcrossWorkflow();
     * @param string $function
     */
    public function check($function = NULL) {
        switch (TRUE) {
            // run a specific checker
            case (in_array($function, $this->availableChecker)):
                $this->runChecker($function);
            break;
            
            // checker not found, show help
            default:
                echo 'Invalid checker "'.$function.'" called !!!'."\n\n".'available checker are:'."\n".'- '.implode("\n- ", $this->availableChecker);
            break;
        }
    }
    
    /**
     * run check function $this->checker{$function}()
     * @param string $function
     */
    protected function runChecker($function) {
        echo '-------------------------------------- '."\n";
        echo '.. run checker "'.$function.'"';
        echo '-------------------------------------- '."\n";
        $tempFunctionName = 'checker'.$function;
        $this->{$tempFunctionName}();
        echo "\n\n";
    }
    
    /**
     * send a test mail to the ADMIN_MAIL adresse
     */
    protected function testerMail() {
        $tempAdresses = implode(', ', (array) App::config()->adminMail);
        if (Mailer::test()) {
            echo('.. successfully send mail to admin "'.$tempAdresses.'". Please check the mail-account if you realy have receieved the mail.');
        }
        else {
            echo('!! error: can not send mail to admin "'.$tempAdresses.'". Please verify the mail server configured in php.ini.');
        }
    }
    
    /**
     * make an entry into a logger
     */
    protected function testerLogger() {
        Logger::logActivity('hello world');
        echo('.. successfully created an entry in logger "'.$logger->getName().'"');
    }
    
}