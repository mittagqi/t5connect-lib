<?php

namespace Lib\Db;

use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Sql;
use Lib\App\App;
use Lib\Logger\Logger;
use stdClass;


/**
 * Db class.
 * See https://docs.laminas.dev/laminas-db/ for details
 * @author sb
 */
class Db {
    
    /**
     * contains the concrete db adapter;
     * @var \Laminas\Db\Adapter\Adapter
     */
    protected $adapter = NULL;
    
    /**
     * contains the db platform object.
     * This object is used for "escaping" etc. 
     * @var \Laminas\Db\Adapter\Platform\AbstractPlatform
     */
    protected $platform = NULL;
    
    
    /**
     * the class will be used as singleton
     * @var \Lib\Db\Db
     */
    protected static $instance = NULL;
    
    protected function __construct() {
        // init the db adapter
        $tempDbConfig = App::config()->db->toArray();
        // aktivate buffer_result, else e.g. DB->result->count() is not available
        $tempDbConfig['options'] = ['buffer_results' => true];
        
        $this->adapter = new \Laminas\Db\Adapter\Adapter($tempDbConfig);
        
        // resolve the platform
        $this->platform = $this->adapter->getPlatform();
    }
    
    /**
     * init the db adapter
     */
    public static function getInstance() {
        if (self::$instance == NULL) {
            self::$instance = new static();
        }
        
        return self::$instance;
    }
    
    /**
     * fire $sql as query to the database and return the statement->execute result
     * @param string $sql
     * @return \Laminas\Db\Adapter\Driver\ResultInterface|NULL
     */
    public static function query($sql) {
        try {
            $db = self::getInstance();
            $statement = $db->adapter->query($sql);
            return $statement->execute();
        }
        catch (\Laminas\Db\Adapter\Exception\RuntimeException $e) {
            Logger::logError(print_r($e, TRUE));
            return NULL;
        }
    }

    /**
     * Returns a Laminas Sql Object
     * @return Sql
     */
    public static function sql(): Sql
    {
        return (new Sql(self::getInstance()->adapter));
    }
    
    /**
     * get one result row from statement $sql
     * @param string $sql
     * @return stdClass
     */
    public static function getRow($sql) {
        $result = self::query($sql);
        
        return ($result) ? $result->current() : NULL;
    }
    
    /**
     * escape $value in the way this db connection needs it in the db statment.
     * Sample: db->escape("lala's") => "lala\'s"
     * @param integer|string|float|... $value
     */
    public static function escape($value) {
        $db = self::getInstance();
        return $db->platform->quoteValue($value);
    }
}


