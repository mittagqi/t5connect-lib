<?php
namespace Lib\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


/**
 * Adds some functionality to the default Symfony Command.
 * 
 * Especially the InputInterface and OutputInterface are replaced by SymfonyStyle in $this->io
 * 
 * 
 */
abstract class AbstractCommand extends Command {
    /**
     * @var InputInterface
     */
    protected $input;
    
    /**
     * @var OutputInterface
     */
    protected $output;
    
    /**
     * @var SymfonyStyle
     */
    protected $io;
    
    
    
    /**
     * Execute the command
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->input = $input;
        $this->output = $output;
        // for detailed infos about SymfonyStyle see https://easysolutionsit.de/artikel/schoenere-kommandos-mit-symfonystyle.html
        $this->io = new SymfonyStyle($input, $output);
        
        return $this->action();
    }
    
    /**
     * The this is the command that will be executed by the symfony execute call.
     * You should return 0 if everything is OK.
     * 
     * For more informations .. 
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    abstract protected function action();
}
